#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_connectButton_clicked();

    void onConnected();
    void onLostConnection();
    void onError(QAbstractSocket::SocketError err);
    void onReadyRead();

    void on_disconnectButton_clicked();

    void on_resButton_clicked();

    void onEditButtonPressed();

    void on_clearButton_clicked();
    void on_address_editingFinished();

    void on_port_editingFinished();

private:
    Ui::MainWindow *ui;

    QTcpSocket *client;

    void addTerminalMessage(QString text);
};

#endif // MAINWINDOW_H
