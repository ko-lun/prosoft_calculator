/**
 * @brief Проект клиента калькулятора
 *
 * Подключается к серверу калькулятора, формирует запрос и отображает ответ.
 * Параметры подключения сохраняются в настройках.
 */

#include "MainWindow.h"
#include <QApplication>
#include <QTextCodec>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    QTextCodec *defaultCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(defaultCodec);

    QCoreApplication::setOrganizationName("Trefilov");
    QCoreApplication::setApplicationName("Calculator");

    MainWindow w;
    w.show();

    return a.exec();
}
