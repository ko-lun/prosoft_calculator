#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDateTime>
#include <QMessageBox>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    client(new QTcpSocket(this))
{
    ui->setupUi(this);

    // Загружаем настройки
    QSettings s;
    ui->address->setText(s.value("address", "localhost").toString());
    ui->port->setValue(s.value("port", 12345).toInt());

    // Приводим кнопки в начальное состояние
    ui->resButton->setDisabled(true);
    ui->disconnectButton->hide();

    connect(client, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
    connect(client, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
    connect(client, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(client, SIGNAL(disconnected()), this, SLOT(onLostConnection()));

    // проще было привязать все эти кнопки к одному обработчику
    connect(ui->n0, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n1, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n2, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n3, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n4, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n5, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n6, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n7, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n8, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->n9, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->nDot, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));

    connect(ui->plus, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->minus, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->mul, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->div, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->open, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
    connect(ui->close, SIGNAL(clicked(bool)), this, SLOT(onEditButtonPressed()));
}

MainWindow::~MainWindow() {
    delete ui;
}

/**
 * @brief MainWindow::on_connectButton_clicked
 * Нажатие кнопки подключения
 *
 * Производит подключение к сервере по указанному адресу и порту, скрывает кнопку подключения и показывает кнопку отключения.
 */
void MainWindow::on_connectButton_clicked() {
    addTerminalMessage(tr("Подключение к серверу").append(" %1:%2").arg(ui->address->text()).arg(ui->port->value()));
    client->connectToHost(ui->address->text(), ui->port->value());
    ui->disconnectButton->show();
    ui->connectButton->hide();
}

/**
 * @brief MainWindow::on_disconnectButton_clicked
 * Нажата кнопка отключения
 *
 * Закрывает соединение с сервером
 */
void MainWindow::on_disconnectButton_clicked() {
    client->close();
}

/**
 * @brief MainWindow::onConnected
 * Успешное подключение к серверу
 *
 * Разрешает кнопку отправки запросов
 */
void MainWindow::onConnected() {
    addTerminalMessage(tr("Успешное подключение к серверу"));
    ui->resButton->setEnabled(true);
}

/**
 * @brief MainWindow::onLostConnection
 * Соединение прервано
 *
 * Запрещает кнопку отправки запросов и отображает кнопку подключения
 */
void MainWindow::onLostConnection() {
    addTerminalMessage(tr("Соединение с сервером закрыто"));
    ui->resButton->setDisabled(true);

    ui->disconnectButton->hide();
    ui->connectButton->show();
}

/**
 * @brief MainWindow::onError
 * Возникла ошибка.
 * @param err
 *
 * Закрывает соединиение, запрещает кнопку отправки запросов и отображает кнопку подключения
 */
void MainWindow::onError(QAbstractSocket::SocketError err) {
    QString text;
    QDebug(&text) << tr("Ошибка соединения: ") << err;
    addTerminalMessage(text);

    client->close();

    ui->disconnectButton->hide();
    ui->connectButton->show();
    ui->resButton->setDisabled(true);
}

/**
 * @brief MainWindow::onReadyRead
 * Готовность входных данных
 *
 * Читает ответ от сервера, разбирает ответ и отображает на экране
 */
void MainWindow::onReadyRead() {
    QString res = QString::fromUtf8(client->readAll());
    addTerminalMessage(tr("Получен ответ: ").append(res));
    bool ok;
    res.toDouble(&ok);
    if (!ok) {
        QMessageBox::warning(this, tr("Калькулятор"), tr("Ошибка в выражении:\n").append(res), QMessageBox::Close, QMessageBox::NoButton);
        ui->result->setText(tr("Ошибка"));
    } else {
        ui->result->setText(res);
    }
}

/**
 * @brief MainWindow::addTerminalMessage
 * Добавление отладочной информации в окне вывода
 * @param text - требуемое сообщение
 *
 * Добавляет строку с датой в окно вывода сообщений
 */
void MainWindow::addTerminalMessage(QString text) {
    ui->terminal->append(QDateTime::currentDateTime().toString(Qt::SystemLocaleShortDate).append(" ").append(text));
}

/**
 * @brief MainWindow::on_resButton_clicked
 * Нажата кнопка "="
 *
 * Отправляет запрос на сервер.
 */
void MainWindow::on_resButton_clicked() {
    client->write(ui->expr->text().toUtf8());
    addTerminalMessage(tr("Отправка запроса: ").append(ui->expr->text()));
}

/**
 * @brief MainWindow::onEditButtonPressed
 * Нажата одна из клавишь редактирования
 *
 * Помещает текст клавиши в текущую позицию.
 */
void MainWindow::onEditButtonPressed() {
    QPushButton *b = qobject_cast<QPushButton *>(sender());
    if (b) {
        ui->expr->insert(b->text());
    }
}

/**
 * @brief MainWindow::on_clearButton_clicked
 * Нажата кнопка очистки строки запроса
 */
void MainWindow::on_clearButton_clicked() {
    ui->expr->clear();
}

/**
 * @brief MainWindow::on_address_editingFinished
 *
 * Сохранение нового адреса в настройки
 */
void MainWindow::on_address_editingFinished() {
    QSettings s;

    s.setValue("address", ui->address->text());
}

/**
 * @brief MainWindow::on_port_editingFinished
 *
 * Сохранение нового порта в настройки
 */
void MainWindow::on_port_editingFinished() {
    QSettings s;

    s.setValue("port", ui->port->value());
}
