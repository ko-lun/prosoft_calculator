#ifndef CALCULATORSERVER_H
#define CALCULATORSERVER_H

#include <QObject>
#include <QTcpServer>

class CalculatorServer : public QObject {
    Q_OBJECT
public:
    explicit CalculatorServer(QObject *parent = 0);

    void listen(int port);
signals:

public slots:
    void onNewConnection();
    void onCloseConnection();
    void onReadyRead();
    void onError(QAbstractSocket::SocketError err);
private:
    QTcpServer *server;
};

#endif // CALCULATORSERVER_H
