#include "Calculator.h"

#include <QObject>
#include <QStack>

using namespace std;

/**
 * @brief Calculator - класс, производящий вычисление значения, заданного в строке.
 *
 * Данный класс разбирает строку на слагаемые, каждое из которых может быть результатом умножения или деления хотя бы одного значения.
 * Значение может быть простым числом или выражением в скобках. В этом случае это выражение рекурсивно разбивается на слагаемые.
 * В результате выдается одно значение или список ошибок, если такие были при разборе строки.
 */

/**
 * @brief Calculator::calculate - разбирает полученную строку на состовные элементы и вычисляет значение.
 * @param expr - выражение в виде строки
 * @return строка с результатом вычисления или список ошибок
 *
 * Затем вызывается разбор слагаемых от данного выражения.
 * В конце не должно остаться каких-либо элементов - выражение должно быть разобрано полностью.
 * Если в ходе разбора строки или вычисления возникли ошибки, то подготавливается строка со списком этих ошибок.
 */
QString Calculator::calculate(QString expr) {
    QStringList list = splitExpression(expr);

    double res = takeExpression(list);
    if (!list.isEmpty()) {
        if (errorString.isEmpty())
            errorString << QObject::tr("Unexpected symbols: ").append(list.join(""));
        res = numeric_limits<double>::quiet_NaN();
    }

    if (isnan(res)) {
        return errorString.join("\n");
    }
    return QString::number(res);
}

/**
 * @brief Calculator::splitExpression - раскладывает выражение на составные элементы
 * @param expr
 * @return QStringList - список отдельных элементов выражения
 *
 * Этот метод убирает все лишние пробелы и разбивает строку на составные элементы.
 *
 */
QStringList Calculator::splitExpression(QString expr) {
    QString item;

    QStringList res;

    foreach (auto ch, expr) {
        if (ch.isDigit() || ch == '.') {
            item.append(ch);
        } else {
            if (!item.isEmpty()) {
                res.append(item);
                item.clear();
            }
            if (!ch.isSpace()) {
                res.append(QString(ch));
            }
        }
    }
    if (!item.isEmpty()) {
        res.append(item);
    }

    return res;
}

/**
 * @brief Calculator::takeExpression - вычисляет сумму всех членов в многочлене
 * @param expr - список элементов
 * @return double - результат вычисления данного выражения или NaN, если выражение прошло с ошибкой.
 *
 * Берет первый член (должен быть обязательно) и добавляет к нему (вычитает из него)
 * следующий член.
 * Выполняет пока не кончится выражение или появятся символы отличные от + или -.
 */
double Calculator::takeExpression(QStringList &expr) {
    // Производим операции сложения и вычитания между всеми членами многочлена
    double res = takeTerm(expr);        // Первый многочлен обязан быть
    if (isnan(res)) {
        return res;
    }

    while (!expr.isEmpty()) {
        // Последующие по возможности
        if (expr.first() == "+") {
            expr.removeFirst();
            double summ = takeTerm(expr);
            if (isnan(summ)) {
                return summ;
            }
            res += summ;
        } else
        if (expr.first() == "-") {
            expr.removeFirst();
            double sub = takeTerm(expr);
            if (isnan(sub)) {
                return sub;
            }
            res -= sub;
        } else {
            break;
        }
    }
    return res;
}

/**
 * @brief Calculator::takeTerm - вычисляет результат умножения или деления всех значений в одном члене
 * @param expr - входное выражение
 * @return double - результат вычисления данного выражения или NaN, если выражение прошло с ошибкой.
 *
 * Берет первое значение в члене (обязательно должно быть) и производит умножение или деление со следующими значениями.
 * * Выполняет пока не кончится выражение или появятся символы отличные от * или /.
 */
double Calculator::takeTerm(QStringList &expr) {
    // Производим операции умножения в каждом из членов
    double res = takeFactor(expr);      // Первое значение обязательно должно быть
    if (isnan(res)) {
        return res;
    }

    while (!expr.isEmpty()) {
        // Остальные по возможности
        if (expr.first() == "*") {
            expr.removeFirst();
            double mux = takeFactor(expr);
            if (isnan(mux)) {
                return mux;
            }
            res *= mux;
        } else
        if (expr.first() == "/") {
            expr.removeFirst();
            double div = takeFactor(expr);
            if (isnan(div)) {
                return div;
            }
            if (div == 0) {
                errorString << QObject::tr("Division by zero");
                return numeric_limits<double>::quiet_NaN();
            }
            res /= div;
        } else {
            break;
        }
    }
    return res;
}

/**
 * @brief Calculator::takeFactor - получить значение
 * @param expr - выражение
 * @return double - результат вычисления данного выражения или NaN, если выражение прошло с ошибкой.
 *
 * Значение может представлять из себя число или выражение в скобках
 */
double Calculator::takeFactor(QStringList &expr) {
    if (expr.isEmpty()) {
        errorString << QObject::tr("Unexpected end of line");
        return numeric_limits<double>::quiet_NaN();
    }
    if (expr.first() == "(") {
        // Это выражение в скобках
        expr.removeFirst();
        double res = takeExpression(expr);
        if (expr.isEmpty() || expr.takeFirst() != ")") {
            errorString << QObject::tr("Missen closing brace");
            return numeric_limits<double>::quiet_NaN();
        }
        return res;
    }
    // Иначе должно быть число
    return takeValue(expr);
}

/**
 * @brief Calculator::takeValue - взять число
 * @param expr
 * @return double - результат вычисления данного выражения или NaN, если выражение прошло с ошибкой.
 *
 * Число может состоять из одного или двух элементов.
 * Добавочные элементы:
 *  1. Знак плюс или минус перед числом
 */
double Calculator::takeValue(QStringList &expr) {
    // Если мы ожидали число, то хотя бы одно значение должно быть
    if (expr.isEmpty()) {
        errorString << QObject::tr("Unexpected end of line");
        return numeric_limits<double>::quiet_NaN();
    }
    QString resStr;

    // Проверка на знак плюс или минус перед числом
    if (expr.first() == "+" || expr.first() == "-") {
        resStr = expr.takeFirst();
    }

    // Снова проверяем на наличие значения
    if (expr.isEmpty()) {
        errorString << QObject::tr("Unexpected end of line");
        return numeric_limits<double>::quiet_NaN();
    }

    // Добавляем значение
    resStr.append(expr.takeFirst());

    bool ok;
    double res = resStr.toDouble(&ok);
    if (!ok) {
        errorString << QObject::tr("Wrong number format: ").append(resStr);
        return numeric_limits<double>::quiet_NaN();
    }
    return res;
}

