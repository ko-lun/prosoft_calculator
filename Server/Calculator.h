#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QByteArray>
#include <QStringList>

class Calculator {
public:
    QString calculate(QString expr);

    QString error() const {
        return errorString.join("\n");
    }

private:

    QStringList splitExpression(QString expr);

    double takeExpression(QStringList &expr);
    double takeTerm(QStringList &expr);
    double takeFactor(QStringList &expr);

    double takeValue(QStringList &expr);

    QStringList errorString;
};

#endif // CALCULATOR_H
