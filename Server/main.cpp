/**
 * @brief Сервер вычисления математических выражений.
 *
 * Программа запускает прослушку TCP порта.
 * Входные данные - математическое выражение.
 * Выходные данные - результат вычисления или ошибки, найденные в выражении.
 * Номер прослушиваемого порта задается в аргументах вызова программы -pXXXXX, где ХХХХХ - номер порта в десятичном виде.
 * По умолчанию используется порт 12345
 */

#include <QCoreApplication>
#include <QTextStream>

#include "Calculator.h"
#include "CalculatorServer.h"

#ifdef UNIT_TEST_ENABLED
/**
 * Для компиляции юнит теста создайте новую конфигурацию сборки и добавьте в этап сборки qmake параметр "CONFIG+=unittest"
 */
#include <QtTest>
#include "CalculatorTst.h"
QTEST_APPLESS_MAIN(CalculatorTst)
#else
int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    // Вычисление номера порта из аргументов программы
    int port = 12345;

    auto listOfArgs = a.arguments();
    foreach (QString arg, listOfArgs) {
        if (arg.left(2) == "-p") {
            bool ok;
            int newPort = arg.remove(0, 2).toInt(&ok);
            if (ok) {
                port = newPort;
            }
        }
    }

    CalculatorServer server;
    server.listen(port);

    return a.exec();
}
#endif

