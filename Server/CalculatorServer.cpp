#include "Calculator.h"
#include "CalculatorServer.h"

#include <QTcpSocket>
#include <QTextStream>
#include <QCoreApplication>
#include <QDateTime>

#include <iostream>

using namespace std;

/**
 * @brief CalculatorServer - класс осуществляющий прослушку заданного порта, рассчет результата заданного выражения и отправку результата
 */

/**
 * @brief qStdOut
 * Возвращает поток стандартного вывода, в который уже выведено текущее время
 * @return QTextStream - ссылка на текстовый поток стандартного вывода
 */
QTextStream& qStdOut() {
    static QTextStream ts(stdout);
    ts << QDateTime::currentDateTime().toString(Qt::SystemLocaleShortDate) << " - ";
    return ts;
}

/**
 * @brief CalculatorServer::CalculatorServer
 * Конструктор
 * @param parent
 *
 * Создает объект сервера и подключается к двум его сигналам:
 * - появление нового соединения
 * - появление ошибки
 */
CalculatorServer::CalculatorServer(QObject *parent) :
    QObject(parent),
    server(new QTcpServer(this))
{
    connect(server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    connect(server, SIGNAL(acceptError(QAbstractSocket::SocketError)), this, SLOT(onError(QAbstractSocket::SocketError)));
}

/**
 * @brief CalculatorServer::listen
 * Открывает порт для прослушивания
 * @param port - номер порта, который требуется прослушивать
 */
void CalculatorServer::listen(int port) {
    qStdOut() << "Start to listen port " << port << endl;

    if (!server->listen(QHostAddress::Any, port)) {
        qStdOut() << "Start listen error: " << server->errorString() << endl;
        qApp->exit(1);
    }
}

/**
 * @brief CalculatorServer::onNewConnection
 * Слот появления нового соединения
 *
 * Обрабатывает все вновь поступившие соединения, соединяя их с сигналами:
 * - получения новых данных
 * - отключения клиента
 */
void CalculatorServer::onNewConnection() {
    while (server->hasPendingConnections()) {
        auto *socket = server->nextPendingConnection();
        connect(socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
        connect(socket, SIGNAL(disconnected()), this, SLOT(onCloseConnection()));

        qStdOut()  << "New connection from ip: " << socket->peerAddress().toString() << endl;
    }
}

/**
 * @brief CalculatorServer::onCloseConnection
 * Слот отключения клиента
 */
void CalculatorServer::onCloseConnection() {
    auto *socket = qobject_cast<QTcpSocket *>(sender());

    if (socket) {
        qStdOut() << "Connection closed. ip: " << socket->peerAddress().toString() << endl;
    }
}

/**
 * @brief CalculatorServer::onReadyRead
 * Слот получения новых данных от клиента
 *
 * Считывает строку запроса, обрабатывает это выражение и отправляет результат обратно клиенту
 */
void CalculatorServer::onReadyRead() {
    auto *socket = qobject_cast<QTcpSocket *>(sender());
    if (socket) {
        QString request = QString::fromUtf8(socket->readAll());

        Calculator calc;

        QString res = calc.calculate(request);
        socket->write(res.toUtf8());

        qStdOut()   << "New request from ip: " << socket->peerAddress().toString() << endl
                    << request << endl
                    << res << endl;

    }
}

/**
 * @brief CalculatorServer::onError
 * Слот возникновения ошибки сервера
 * @param err
 */
void CalculatorServer::onError(QAbstractSocket::SocketError err) {
    qStdOut() << "Error detected: " << err << endl;
}

