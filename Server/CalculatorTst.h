#ifndef CALCULATORTST_H
#define CALCULATORTST_H

#include "Calculator.h"

#include <QObject>

class CalculatorTst : public QObject {
    Q_OBJECT
public:
    explicit CalculatorTst(QObject *parent = 0);

private Q_SLOTS:
    void testValue();
    void testWrongValue();
    void testSumm();
    void testMul();
    void testDifficult();
    void testBrace();
    void testEndOfLine();
};

#endif // CALCULATORTST_H
