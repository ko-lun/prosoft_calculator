#include "CalculatorTst.h"

#include <QtTest>

CalculatorTst::CalculatorTst(QObject *parent) : QObject(parent) {

}

void CalculatorTst::testValue() {
    QVERIFY(Calculator().calculate("512") == "512");
    QVERIFY(Calculator().calculate("\t512  \n") == "512");
    QVERIFY(Calculator().calculate("-28") == "-28");
}

void CalculatorTst::testWrongValue() {
    QVERIFY(Calculator().calculate("+-512") == "Wrong number format: +-");
    QVERIFY(Calculator().calculate("+512.z") == "Unexpected symbols: z");
    QVERIFY(Calculator().calculate("-512.4.5") == "Wrong number format: -512.4.5");
}

void CalculatorTst::testSumm() {
    QVERIFY(Calculator().calculate("2 +   \t 3") == "5");
    QVERIFY(Calculator().calculate("5-3") == "2");
    QVERIFY(Calculator().calculate("5-(8)") == "-3");
    QVERIFY(Calculator().calculate("5-(8+5)") == "-8");
}

void CalculatorTst::testMul() {
    QVERIFY(Calculator().calculate("2*3") == "6");
    QVERIFY(Calculator().calculate("15/5") == "3");
    QVERIFY(Calculator().calculate("12.2*5") == "61");
}

void CalculatorTst::testDifficult() {
    QVERIFY(Calculator().calculate("2*2+2") == "6");
    QVERIFY(Calculator().calculate("2+2*2") == "6");
    QVERIFY(Calculator().calculate("(2+2)*2") == "8");
}

void CalculatorTst::testBrace() {
    QVERIFY(Calculator().calculate("2*(2+3") == "Missen closing brace");
}

void CalculatorTst::testEndOfLine() {
    QVERIFY(Calculator().calculate("") == "Unexpected end of line");
    QVERIFY(Calculator().calculate("+") == "Unexpected end of line");
    QVERIFY(Calculator().calculate("2+") == "Unexpected end of line");
    QVERIFY(Calculator().calculate("(2+5))") == "Unexpected symbols: )");
}

