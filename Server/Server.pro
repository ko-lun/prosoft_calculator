QT += core network
QT -= gui

TARGET = Server
CONFIG += console
CONFIG -= app_bundle

unittest {
    QT       += testlib
    DEFINES += UNIT_TEST_ENABLED
    SOURCES += CalculatorTst.cpp
    HEADERS += CalculatorTst.h
}

CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp \
    CalculatorServer.cpp \
    Calculator.cpp


HEADERS += \
    CalculatorServer.h \
    Calculator.h


